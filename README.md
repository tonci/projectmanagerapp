Fork from https://github.com/delordson/ProjectManagerApp.git by Delordson Kallon

- The Project Manager App targeted at small businesses who take on projects from a number of clients. The Project Manager App allows a business to monitor projects, assign staff resources to those projects, assign tasks to resources linked to a project and manage risks associated with the project. 
- upgraded with VS2015


## Project Manager App

![](Resources/ProjectsMain.png)   The Project Manager App targeted at small businesses who take on projects from a number of clients. The Project Manager App allows a business to monitor projects, assign staff resources to those projects, assign tasks to resources linked to a project and manage risks associated with the project. This is a fully featured complete LightSwitch html5 line of business application which you can extend and adapt to your specific needs.  

## Modules

The Project Manager App Modules includes a Home Page, the All Projects Module,  the Clients Module, the My Projects Module, the My Tasks Module and the Settings Module.  

## Access

Access is controlled through a log in screen. The administrator uses an associated Silverlight application to configure new users, create roles and permissions for those roles as well as to assign users to roles. [![](Resources/screenshot_11012013_175304.png "screenshot_11012013_175304")](Resources/screenshot_11012013_175304.png)  

## The Home Screen

Access to all the features of the Project Manager App is from the home page. From here, users can navigate to the ‘All Projects Module’, the ‘Clients Module’, the ‘My Projects Module’, the ‘My Tasks Module’  and the ‘Settings Module’. [![](Resources/screenshot_11012013_175340.png "screenshot_11012013_175340")](Resources/screenshot_11012013_175340.png)  

## All Projects

Clicking or tabbing on the All Projects button takes the user to the ‘Browse Projects’ Page. On this page, the user of presented with a list of all projects for the business. A search box is present for those businesses lucky enough to have so many projects they need to filter them ![Smile](file:///C:/Users/Delordson/AppData/Local/Temp/WindowsLiveWriter1286139640/supfiles34B6587/wlEmoticon-smile2.png).   [![](Resources/screenshot_11012013_175347.png "screenshot_11012013_175347")](Resources/screenshot_11012013_175347.png)     Clicking or tabbing on any project listed takes the user to a page for managing that project. This page provides for full details of the projects, including a start and end date. The page is laid out in a series of tabs. The first tab presents key information about the project such as its reference number, title, description, stat and end dates, the client, the project manager, the project status and importantly, the budget.   [![](Resources/screenshot_11012013_175426.png "screenshot_11012013_175426")](Resources/screenshot_11012013_175426.png)   Of course the built in LightSwitch validation of required fields and field lengths works great as expected… [![](Resources/screenshot_11012013_175842.png "screenshot_11012013_175842")](Resources/screenshot_11012013_175842.png)   …but we’ve also added multi field validation where it makes sense. So you can’t end a project before it has started. [![](Resources/screenshot_11012013_175915.png "screenshot_11012013_175915")](Resources/screenshot_11012013_175915.png)     The Project Tasks tab is where the full set of tasks required to complete the project are listed. [![](Resources/screenshot_11012013_175433.png "screenshot_11012013_175433")](Resources/screenshot_11012013_175433.png)   The Project Resources Tab shows the list of staff who have been assigned to the project.   [![](Resources/screenshot_11012013_175510.png "screenshot_11012013_175510")](Resources/screenshot_11012013_175510.png)   Any employee who gets assigned to project is automatically emailed to let them know they have been so assigned.   [![](Resources/ProjectAssignment.png "ProjectAssignment")](Resources/ProjectAssignment.png)   Finally the Project Risks Tab is where risks to to the completion of the project and associated mitigations are recorded. [![](Resources/screenshot_11012013_175514.png "screenshot_11012013_175514")](Resources/screenshot_11012013_175514.png)     Again full validation is enabled… [![](Resources/screenshot_11012013_175523.png "screenshot_11012013_175523")](Resources/screenshot_11012013_175523.png)     Back in the Project Task tab, Tabbing or clicking on any of the listed tasks takes the user to a page for managing that task. This task management page is laid out in two tabs. A details tab which lists describes the task and includes a date task due… [![](Resources/screenshot_11012013_175436.png "screenshot_11012013_175436")](Resources/screenshot_11012013_175436.png)     …and task resources can be assigned from the list of resources already assigned to the project.     Again any resource who is allocated a task is informed by email… [![](Resources/TaskAssignment.png "TaskAssignment")](Resources/TaskAssignment.png)    

## Clients

The client module allows users to view a full list of clients for whom projects have been or are being undertaken. [![](Resources/screenshot_11012013_175643.png "screenshot_11012013_175643")](Resources/screenshot_11012013_175643.png)     Clicking or tabbing on any of these clients takes the user to a client management page. This page is laid out in three tabs. A details tab where key details of the client such as name and address are lists… [![](Resources/screenshot_11012013_175647.png "screenshot_11012013_175647")](Resources/screenshot_11012013_175647.png)     A client contacts tab where key personnel at the client are listed and can be managed… [![](Resources/screenshot_11012013_175650.png "screenshot_11012013_175650")](Resources/screenshot_11012013_175650.png)   [![](Resources/screenshot_11012013_175654.png "screenshot_11012013_175654")](Resources/screenshot_11012013_175654.png)     …and a client projects tab, which lists projects for that client. Tabbing of clicking on any client in this list takes the user to the project management page described above. [![](Resources/screenshot_11012013_175658.png "screenshot_11012013_175658")](Resources/screenshot_11012013_175658.png)    

## My Projects

The My projects module simply lists all the projects to which the currently logged in user has been assigned. Only your projects are visible from this screen. Clicking or tabbing on any of these projects takes the user to the project management page described above. [![](Resources/screenshot_11012013_175707.png "screenshot_11012013_175707")](Resources/screenshot_11012013_175707.png)  

## My Tasks

The My Tasks module simply lists all the tasks across all projects, which have been assigned to the currently logged in user. Only your tasks are visible from this screen. Clicking or tabbing on any of these tasks takes the user to the task management page described above.   [![](Resources/screenshot_11012013_175713.png "screenshot_11012013_175713")](Resources/screenshot_11012013_175713.png)  

## Settings

The Settings Module is the key module used by the administrator and any other users given the right permissions for managing key application reference data. The main item managed is the list of employees. [![](Resources/screenshot_11012013_175719.png "screenshot_11012013_175719")](Resources/screenshot_11012013_175719.png)   **Employees** [![](Resources/screenshot_11012013_175756.png "screenshot_11012013_175756")](Resources/screenshot_11012013_175756.png)     The Employees page is laid out in two tabs. A details tab which provides key information such as name and email… [![](Resources/screenshot_11012013_175800.png "screenshot_11012013_175800")](Resources/screenshot_11012013_175800.png)   …and a skills tab which lists some of the skills a project manager may need want to review before assigning an Employee to a project or to a task in that projects. [![](Resources/screenshot_11012013_175805.png "screenshot_11012013_175805")](Resources/screenshot_11012013_175805.png)    

## Summary

The Project Manager App is a fully featured complete LightSwitch html5 line of business application which you can extend and adapt to your specific needs.